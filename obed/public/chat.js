// make connection
var socket  =io.connect('http://localhost:4000');

//Query DOM
var message=document.getElementById('msg');
var handle=document.getElementById('handle');
var btn=document.getElementById('send');
var output=document.getElementById('output');
var feedback = document.getElementById('feedback');
var questioner = {};

//Emit events

btn.addEventListener('click',function(){
    socket.emit('chat',{
        message:message.value,
        handle:handle.value
    });
    questioner = { handle: handle.value, message: message.value };
});
// message.addEventListener('keypress',function(){
//     socket.emit('typing',handle.value);
// });

//listen for events
socket.on('chatt',function(info){
    // feedback.innerHTML =""
    feedback.innerHTML +='<p><strong>'+info.handle +': </strong>' +info.message + '</p>'
    output.innerHTML +='<p><strong>'+ questioner.handle +': </strong>' + questioner.message + '</p>'
});
// socket.on('typing',function(data){
//     feedback.innerHTML='<p><em>'+data +'is typing a message.....</em></p>'
// });