from chatterbot import ChatBot #import the chatbot
from chatterbot.trainers import ListTrainer #method to train the chatbot
import os
def train():
    bot = ChatBot ('Test')# create the chatbot
    bot.set_trainer(ListTrainer) #set the trainer
    
    for _file in os.listdir('files'):
        chats = open("files/" + _file, 'r').readlines()
    
    bot.train(chats)
    return bot
# while True:
    # request = input('You:')
    # response = bot.get_response(request)
    # print('Bot:',response)