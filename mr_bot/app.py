from flask import Flask,request
import json
import ChatBot
from ChatBot import train

app = Flask(__name__)
global bot

def chatter(question,bot):
    answer = bot.get_response(question)
    return answer

@app.route('/',methods=['POST'])
def index():
    data = request.get_json()
    question = data.get('qn')
    answer = chatter(question,bot)
    print(answer)
    return json.dumps({'answer': answer.serialize()})




if __name__ == "__main__":
    bot = train()
    app.run(port=6000,debug=True)